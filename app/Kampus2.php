<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kampus2 extends Model
{
    protected $table = 'kampus2';
    protected $fillable = [
        'NamaKampus', 'NamaRektor', 'Alamat', 'NoTelp', 'JumlahFakultas', 'TanggalOperasi'
    ];
    protected $primaryKey = 'IdKampus';

    public $timestamps = false;
}

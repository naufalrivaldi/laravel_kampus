<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KampusRequest;

use App\Kampus;

class KampusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['no'] = 1;
        $data['kampus'] = Kampus::all();
        return view('page.kampus.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kampus'] = (object)[
            'id' => '',
            'nama' => '',
            'alamat' => ''
        ];
        return view('page.kampus.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KampusRequest $request)
    {
        Kampus::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat
        ]);

        return redirect()->route('kampus.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kampus'] = Kampus::find($id);

        return view('page.kampus.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KampusRequest $request, $id)
    {
        $data = Kampus::find($id);
        $data->nama = $request->nama;
        $data->alamat = $request->alamat;
        $data->save();

        return redirect()->route('kampus.index')->with('success', 'Update data berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kampus::find($id);
        $data->delete();
        return redirect()->route('kampus.index')->with('success', 'Delete data berhasil!');
    }
}

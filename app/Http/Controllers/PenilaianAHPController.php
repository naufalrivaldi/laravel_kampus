<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Penilaian;
use App\DetailPenilaian;
use App\Kriteria;
use App\Kampus;

use DB;

class PenilaianAHPController extends Controller
{
    public function index(){
        $data['no'] = '1';
        $data['penilaian'] = Penilaian::where('kategori', '1')->orderBy('hasil', 'desc')->get();

        return  view('page.penilaian.ahp.index', $data);
    }

    public function form(){
        $data['kriteria'] = kriteria::where('kategori', '1')->get();
        $data['kampus'] = Kampus::orderBy('nama', 'asc')->get();

        return view('page.penilaian.ahp.form', $data);
    }

    public function view($id){
        $data['no'] = 1;
        $data['penilaian'] = Penilaian::find($id);
         return view('page.penilaian.ahp.view', $data);
    }

    public function store(Request $request){
        Penilaian::where('kategori', '1')->delete();

        $nilai = 0;
        $hasil = 0;
        $kriteria = Kriteria::where('kategori', '1')->get();
        $kampus = Kampus::all();

        foreach($kampus as $kry){
            Penilaian::create([
                'kampusId' => $kry->id,
                'kategori' => '1'
            ]);
            $penilaian = Penilaian::where('kategori', '1')->where('kampusId', $kry->id)->first();
            foreach($kriteria as $krt){
                $nilai = $krt->bobot * $request->nilai[$kry->id][$krt->id];
                $hasil += $nilai;
                DetailPenilaian::create([
                    'nilai' => $nilai,
                    'kriteriaId' => $krt->id,
                    'penilaianId' => $penilaian->id
                ]);
            }
            
            $penilaian->hasil = $hasil;
            $penilaian->save();
            $hasil = 0;
        }

        return redirect()->route('penilaian.ahp.index')->with('success', 'Berhasil melakukan penilaian.');
    }
}

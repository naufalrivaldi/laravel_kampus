<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Penilaian;
use App\DetailPenilaian;
use App\Kriteria;
use App\Kampus;

class PenilaianSAWController extends Controller
{
    public function index(){
        $data['no'] = '1';
        $data['penilaian'] = Penilaian::where('kategori', '2')->orderBy('hasil', 'desc')->get();

        return  view('page.penilaian.saw.index', $data);
    }

    public function form(){
        $data['kriteria'] = kriteria::where('kategori', '2')->get();
        $data['kampus'] = Kampus::orderBy('nama', 'asc')->get();

        return view('page.penilaian.saw.form', $data);
    }

    public function view($id){
        $data['no'] = 1;
        $data['penilaian'] = Penilaian::find($id);
         return view('page.penilaian.saw.view', $data);
    }

    public function store(Request $request){
        Penilaian::where('kategori', '2')->delete();
        
        $kampus = Kampus::all();
        $kriteria = Kriteria::where('kategori', '2')->get();
        $max = $this->max($request, $kampus, $kriteria);
        $nilai = $request->nilai;
        $total = 0;
        $hasil = 0;
        // dd($max);
        // normalisasi
        foreach($kampus as $kry){
            foreach($kriteria as $krt){
                $nilai[$kry->id][$krt->id] = $nilai[$kry->id][$krt->id] / $max[$krt->id];
            }
        }

        // pembobotan + hasil
        foreach($kampus as $kry){
            Penilaian::create([
                'kampusId' => $kry->id,
                'kategori' => '2'
            ]);
            $penilaian = Penilaian::where('kategori', '2')->where('kampusId', $kry->id)->first();

            foreach($kriteria as $krt){
                $nilai[$kry->id][$krt->id] = $nilai[$kry->id][$krt->id] * $krt->bobot / 100;
                $hasil +=  $nilai[$kry->id][$krt->id];
                DetailPenilaian::create([
                    'nilai' => $nilai[$kry->id][$krt->id],
                    'kriteriaId' => $krt->id,
                    'penilaianId' => $penilaian->id
                ]);
            }
            
            $penilaian->hasil = $hasil;
            $penilaian->save();
            $hasil = 0;
        }

        return redirect()->route('penilaian.saw.index')->with('success', 'Berhasil melakukan penilaian.');
    }

    public function max($request, $kampus, $kriteria){
        $tmp = 0;
        $max = [];
        foreach($kriteria as $krt){
            foreach($kampus as $kry){
                if($tmp < $request->nilai[$kry->id][$krt->id]){
                    $tmp = $request->nilai[$kry->id][$krt->id];   
                }
            }
            $max[$krt->id] = $tmp;
            $tmp = 0;
        }

        return $max;
    }
}

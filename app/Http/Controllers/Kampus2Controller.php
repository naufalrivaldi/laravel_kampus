<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Kampus2Request;

use App\Kampus2;

class Kampus2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['no'] = 1;
        $data['kampus2'] = Kampus2::all();
        return view('page.kampus2.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kampus2'] = (object)[
            'IdKampus' => '',
            'NamaKampus' => '',
            'NamaRektor' => '',
            'Alamat' => '',
            'NoTelp' => '',
            'JumlahFakultas' => '',
            'TanggalOperasi' => ''
        ];
        return view('page.kampus2.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Kampus2Request $request)
    {
        Kampus2::create([
            'NamaKampus' => $request->NamaKampus,
            'NamaRektor' => $request->NamaRektor,
            'Alamat' => $request->Alamat,
            'NoTelp' => $request->NoTelp,
            'JumlahFakultas' => $request->JumlahFakultas,
            'TanggalOperasi' => $request->TanggalOperasi
        ]);

        return redirect()->route('kampus2.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kampus2'] = Kampus2::find($id);

        return view('page.kampus2.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Kampus2Request $request, $id)
    {
        $data = Kampus2::find($id);
        $data->NamaKampus = $request->NamaKampus;
        $data->NamaRektor = $request->NamaRektor;
        $data->Alamat = $request->Alamat;
        $data->NoTelp = $request->NoTelp;
        $data->JumlahFakultas = $request->JumlahFakultas;
        $data->TanggalOperasi = $request->TanggalOperasi;
        $data->save();

        return redirect()->route('kampus2.index')->with('success', 'Update data berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kampus2::find($id);
        $data->delete();
        return redirect()->route('kampus2.index')->with('success', 'Delete data berhasil!');
    }
}

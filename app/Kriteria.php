<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model
{
    protected $table = 'kriteria';
    protected $fillable = [
    	'nama', 'kategori', 'bobot'
    ];
    public $timestamps = false;

    public function subKriteria(){
    	return $this->hasMany('App\SubKriteria', 'kriteriaId');
    }

    public function detailPenilaian(){
    	return $this->hasMany('App\DetailPenilaian', 'kriteriaId');
    }
}

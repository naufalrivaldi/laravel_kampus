<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubKriteria extends Model
{
    protected $table = 'sub_kriteria';
    protected $fillable = [
    	'nama', 'bobot', 'kriteriaId'
    ];
    public $timestamps = false;

    public function kriteria(){
    	return $this->belongsTo('App\Kriteria', 'kriteriaId');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kampus extends Model
{
    protected $table = 'kampus';
    protected $fillable = [
    	'nama', 'jk', 'tglLahir', 'alamat'
    ];

    public function penilaian(){
    	return $this->hasMany('App\Penilaian', 'kampusId');
    }
}

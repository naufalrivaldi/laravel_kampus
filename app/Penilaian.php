<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $table = 'penilaian';
    protected $fillable = [
    	'hasil', 'kategori', 'kampusId'
    ];

    public function kampus(){
    	return $this->belongsTo('App\Kampus', 'kampusId');
    }

    public function detailPenilaian(){
    	return $this->hasMany('App\detailPenilaian', 'penilaianId');
    }
}

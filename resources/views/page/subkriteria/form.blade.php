@extends('master')

@section('content')

<h5>Form Sub Kriteria (AHP)</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('subkriteria.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ (empty($subkriteria->id))?route('subkriteria.store'):route('subkriteria.update', ['subkriterium' => $subkriteria->id]) }}" method="POST">
          @csrf
          @if(!empty($subkriteria->id))
            @method('PUT')
          @endif

          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control {{ ($errors->has('nama'))?'is-invalid':'' }}" value="{{ $subkriteria->nama }}">

            <!-- error -->
            @if($errors->has('nama'))
              <small class="text-danger">*{{ $errors->first('nama') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="kriteriaId">Kriteria</label>
            <select name="kriteriaId" id="kriteriaId" class="form-control {{ ($errors->has('kriteriaId'))?'is-invalid':'' }}">
              <option value="">Pilih</option>
              @foreach($kriteria as $kr)
                <option value="{{ $kr->id }}" {{ ($subkriteria->kriteriaId == $kr->id)?'selected':'' }}>{{ $kr->nama }}</option>
              @endforeach
            </select>

            <!-- error -->
            @if($errors->has('kriteriaId'))
              <small class="text-danger">*{{ $errors->first('kriteriaId') }}</small>
            @endif
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
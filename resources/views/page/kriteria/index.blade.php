@extends('master')

@section('content')

<h5>Data Kriteria</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('kriteria.create') }}" class="btn btn-primary">Tambah Data</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Kategori</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($kriteria as $row)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $row->nama }}</td>
                  <td><span class="badge badge-success">{{ kategori($row->kategori) }}</span></td>
                  <td>
                    <form action="{{ route('kriteria.destroy', ['kriterium' => $row->id]) }}" method="POST" onSubmit="return confirm('Anda yakin ingin menghapus data?')">
                      @csrf
                      @method('DELETE')
                      <a href="{{ route('kriteria.edit', ['kriterium' => $row->id]) }}" class="btn btn-success btn-sm">Edit</a>
                      <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                    </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
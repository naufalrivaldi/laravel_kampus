@extends('master')

@section('content')

<h5>Form Kampus2</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('kampus2.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ (empty($kampus2->IdKampus))?route('kampus2.store'):route('kampus2.update', ['kampus2' => $kampus2->IdKampus]) }}" method="POST">
          @csrf
          @if(!empty($kampus2->IdKampus))
            @method('PUT')
          @endif

          <div class="form-group">
            <label for="NamaKampus">Nama</label>
            <input type="text" name="NamaKampus" id="NamaKampus" class="form-control {{ ($errors->has('NamaKampus'))?'is-invalid':'' }}" value="{{ $kampus2->NamaKampus }}">

            <!-- error -->
            @if($errors->has('NamaKampus'))
              <small class="text-danger">*{{ $errors->first('NamaKampus') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="NamaRektor">Rektor</label>
            <input type="text" name="NamaRektor" id="NamaRektor" class="form-control {{ ($errors->has('NamaRektor'))?'is-invalid':'' }}" value="{{ $kampus2->NamaRektor }}">

            <!-- error -->
            @if($errors->has('NamaRektor'))
              <small class="text-danger">*{{ $errors->first('NamaRektor') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="Alamat">Alamat</label>
            <textarea name="Alamat" id="Alamat" rows="5" class="form-control {{ ($errors->has('Alamat'))?'is-invalid':'' }}">{{ $kampus2->Alamat }}</textarea>

            <!-- error -->
            @if($errors->has('Alamat'))
              <small class="text-danger">*{{ $errors->first('Alamat') }}</small>
            @endif
          </div>
          
          <div class="form-group">
            <label for="NoTelp">Telp</label>
            <input type="number" name="NoTelp" id="NoTelp" class="form-control {{ ($errors->has('NoTelp'))?'is-invalid':'' }}" value="{{ $kampus2->NoTelp }}">

            <!-- error -->
            @if($errors->has('NoTelp'))
              <small class="text-danger">*{{ $errors->first('NoTelp') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="JumlahFakultas">Jumlah Fakultas</label>
            <input type="number" name="JumlahFakultas" id="JumlahFakultas" class="form-control {{ ($errors->has('JumlahFakultas'))?'is-invalid':'' }}" value="{{ $kampus2->JumlahFakultas }}">

            <!-- error -->
            @if($errors->has('JumlahFakultas'))
              <small class="text-danger">*{{ $errors->first('JumlahFakultas') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="TanggalOperasi">Tanggal Operasi</label>
            <input type="date" name="TanggalOperasi" id="TanggalOperasi" class="form-control {{ ($errors->has('TanggalOperasi'))?'is-invalid':'' }}" value="{{ $kampus2->TanggalOperasi }}">

            <!-- error -->
            @if($errors->has('TanggalOperasi'))
              <small class="text-danger">*{{ $errors->first('TanggalOperasi') }}</small>
            @endif
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
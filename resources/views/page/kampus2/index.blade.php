@extends('master')

@section('content')

<h5>Data Kampus2</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('kampus2.create') }}" class="btn btn-primary">Tambah Data</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Rektor</th>
                <th>Alamat</th>
                <th>No Telp</th>
                <th>Jml Fakultas</th>
                <th>Tanggal Operasi</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($kampus2 as $row)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $row->NamaKampus }}</td>
                  <td>{{ $row->NamaRektor }}</td>
                  <td>{{ $row->Alamat }}</td>
                  <td>{{ $row->NoTelp }}</td>
                  <td>{{ $row->JumlahFakultas }}</td>
                  <td>{{ $row->TanggalOperasi }}</td>
                  <td>
                    <form action="{{ route('kampus2.destroy', ['kampus2' => $row->IdKampus]) }}" method="POST" onSubmit="return confirm('Anda yakin ingin menghapus data?')">
                      @csrf
                      @method('DELETE')
                      <a href="{{ route('kampus2.edit', ['kampus2' => $row->IdKampus]) }}" class="btn btn-success btn-sm">Edit</a>
                      <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                    </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
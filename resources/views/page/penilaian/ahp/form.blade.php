@extends('master')

@section('content')

<h5>Form Sub Kriteria (AHP)</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('penilaian.ahp.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ route('penilaian.ahp.store') }}" method="POST">
          @csrf

          <table class="table table-bordered">
            <tr>
              <td>No</td>
              <td>Nama</td>
              @foreach($kriteria as $row)
                <td>{{ $row->nama }}</td>
              @endforeach
            </tr>
            @php $no = 1; @endphp
            @foreach($kampus as $row)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $row->nama }}</td>
                @foreach($kriteria as $kr)
                  <td>
                    <select name="nilai[{{ $row->id }}][{{ $kr->id }}]" class="form-control" required>
                      <option value="">Pilih</option>
                      @foreach($kr->subKriteria as $sk)
                        <option value="{{ $sk->bobot }}">{{ $sk->nama }}</option>
                      @endforeach
                    </select>
                  </td>
                @endforeach
              </tr>
            @endforeach
          </table>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
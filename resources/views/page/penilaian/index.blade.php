@extends('master')

@section('content')

<h5>Penilaian Metode SPK</h5>
<div class="row">
	<div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>AHP</h3>
      </div>
      <div class="card-body">
        <a href="{{ route('penilaian.ahp.index') }}" class="btn btn-primary btn-block">Nilai</a>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>SAW</h3>
      </div>
      <div class="card-body">
        <a href="{{ route('penilaian.saw.index') }}" class="btn btn-primary btn-block">Nilai</a>
      </div>
    </div>
  </div>
</div>

@endsection
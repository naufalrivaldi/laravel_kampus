@extends('master')

@section('content')

<h5>Form Sub Kriteria (SAW)</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('penilaian.saw.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <div class="alert alert-info" role="alert">
          <ul>
            <li>Nilai diinputkan rentang 1 - 100</li>
          </ul>
        </div>
        <form action="{{ route('penilaian.saw.store') }}" method="POST">
          @csrf

          <table class="table table-bordered">
            <tr>
              <td>No</td>
              <td>Nama Kampus</td>
              @foreach($kriteria as $row)
                <td>{{ $row->nama.' ('.$row->bobot.'%)'}}</td>
              @endforeach
            </tr>
            @php  $no = 1; @endphp
            @foreach($kampus as $row)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $row->nama }}</td>
                @foreach($kriteria as $kr)
                  <td>
                    <input type="number" name="nilai[{{ $row->id }}][{{ $kr->id }}]" class="form-control" min="1" max="100" required>
                  </td>
                @endforeach
              </tr>
            @endforeach
          </table>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
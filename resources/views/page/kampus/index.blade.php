@extends('master')

@section('content')

<h5>Data Kampus</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('kampus.create') }}" class="btn btn-primary">Tambah Data</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($kampus as $row)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $row->nama }}</td>
                  <td>{{ $row->alamat }}</td>
                  <td>
                    <form action="{{ route('kampus.destroy', ['kampus' => $row->id]) }}" method="POST" onSubmit="return confirm('Anda yakin ingin menghapus data?')">
                      @csrf
                      @method('DELETE')
                      <a href="{{ route('kampus.edit', ['kampus' => $row->id]) }}" class="btn btn-success btn-sm">Edit</a>
                      <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                    </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
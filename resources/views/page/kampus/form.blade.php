@extends('master')

@section('content')

<h5>Form Kampus</h5>
<div class="row">
	<div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('kampus.index') }}" class="btn btn-success">Kembali</a>
      </div>
      <div class="card-body">
        <form action="{{ (empty($kampus->id))?route('kampus.store'):route('kampus.update', ['kampus' => $kampus->id]) }}" method="POST">
          @csrf
          @if(!empty($kampus->id))
            @method('PUT')
          @endif

          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" class="form-control {{ ($errors->has('nama'))?'is-invalid':'' }}" value="{{ $kampus->nama }}">

            <!-- error -->
            @if($errors->has('nama'))
              <small class="text-danger">*{{ $errors->first('nama') }}</small>
            @endif
          </div>

          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="alamat" rows="5" class="form-control {{ ($errors->has('alamat'))?'is-invalid':'' }}">{{ $kampus->alamat }}</textarea>

            <!-- error -->
            @if($errors->has('alamat'))
              <small class="text-danger">*{{ $errors->first('alamat') }}</small>
            @endif
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
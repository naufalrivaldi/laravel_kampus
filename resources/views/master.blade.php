<!doctype html>
<html lang="en">
  <head>
    @include('layout.header')
  </head>
  <body>
    @include('layout.navbar')

    <!-- konten -->
    <div class="container" style="margin-top: 5%">
      @include('layout.alert')
      @yield('content')
    </div>
    <!-- konten -->


    @yield('modal')
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    @include('layout.javascript')
    @yield('script')
  </body>
</html>
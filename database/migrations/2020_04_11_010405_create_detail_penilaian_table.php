<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_penilaian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('nilai');
            $table->unsignedBigInteger('kriteriaId');
            $table->unsignedBigInteger('penilaianId');

            // fk
            $table->foreign('kriteriaId')
                    ->references('id')
                    ->on('kriteria')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->foreign('penilaianId')
                    ->references('id')
                    ->on('penilaian')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_penilaian');
    }
}

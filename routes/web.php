<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'KampusController@index');

Route::resource('kampus', 'KampusController');
Route::resource('kampus2', 'Kampus2Controller');
Route::resource('kriteria', 'KriteriaController');
Route::resource('subkriteria', 'SubKriteriaController');

Route::group(['prefix' => 'bobot'], function(){
  Route::get('/', 'BobotController@index')->name('bobot.index');

  Route::group(['prefix' => 'ahp'], function(){
    Route::get('/', 'BobotController@ahp')->name('bobot.ahp');
    Route::get('form', 'BobotController@ahpform')->name('bobot.ahp.form');
    Route::post('/', 'BobotController@ahpstore')->name('bobot.ahp.store');

    Route::group(['prefix' => 'sub'], function(){
      Route::get('/{kriteriaId}/form', 'BobotController@subForm')->name('bobot.ahp.sub.form');
      Route::post('/', 'BobotController@substore')->name('bobot.ahp.sub.store');
    });
  });

  Route::group(['prefix' => 'saw'], function(){
    Route::get('/', 'BobotController@saw')->name('bobot.saw');
    Route::get('form', 'BobotController@sawform')->name('bobot.saw.form');
    Route::post('/', 'BobotController@sawstore')->name('bobot.saw.store');
  });
});

Route::group(['prefix' => 'penilaian'], function(){
  Route::get('/', 'PenilaianController@index')->name('penilaian.index');

  Route::group(['prefix' => 'ahp'], function(){
    Route::get('/', 'PenilaianAHPController@index')->name('penilaian.ahp.index');
    Route::get('/form', 'PenilaianAHPController@form')->name('penilaian.ahp.form');
    Route::get('/{id}/view', 'PenilaianAHPController@view')->name('penilaian.ahp.view');
    Route::post('/', 'PenilaianAHPController@store')->name('penilaian.ahp.store');
  });

  Route::group(['prefix' => 'saw'], function(){
    Route::get('/', 'PenilaianSAWController@index')->name('penilaian.saw.index');
    Route::get('/form', 'PenilaianSAWController@form')->name('penilaian.saw.form');
    Route::get('/{id}/view', 'PenilaianSAWController@view')->name('penilaian.saw.view');
    Route::post('/', 'PenilaianSAWController@store')->name('penilaian.saw.store');
  });
});
